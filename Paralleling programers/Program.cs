﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paralleling_programers
{
    class Program
    {
        static void Main(string[] args)
        {
            Task task = new Task(() => Console.WriteLine("task1 is executed"));
            task.Start();

            Task task2 = Task.Factory.StartNew(() => Console.WriteLine("Task 2 is executed"));

            Task task3 = Task.Run(() => Console.WriteLine("Task3 is executed"));
            Console.ReadKey();
        }
    }
}
