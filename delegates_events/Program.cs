﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delegates_events
{

    class Program
    {
        static void Main(string[] args)
        {
            var boozer = new Boozer();
            boozer.oilEnded += OnOilEnded;

            var gopnik = new Gopnik();
            gopnik.oilEnded += OnOilEnded;

            gopnik.LetsGoShelkat();
            boozer.LetsGo();
            Console.ReadKey();
        }

        private static void OnOilEnded(object sender, EventArgs args)
        {
            if (sender is Gopnik)
            {
                Console.WriteLine("семки есть?");
            }
            else if (sender is Boozer)
            {
                Console.WriteLine("пора в магаз за добавкой");
            }


    }

    class Boozer
    {
        public int BoozeAmount { get; set; }

        public event EventHandler oilEnded;

        public Boozer()
        {
            BoozeAmount = 100;
        }

        private void Worker()
        {
            for (int i = BoozeAmount; i >= 0; i--)
            {
                if (BoozeAmount == 0)
                {
                    if (oilEnded != null)
                    {
                        oilEnded(this, new EventArgs());
                    }
                }
                BoozeAmount--;
            }
        }
        public void LetsGo()
        {
            Worker();
        }

        }
    }

    class Gopnik
    {
        public int SemkiAmount { get; set; }

        public event EventHandler oilEnded;
        public Gopnik()
        {
            SemkiAmount = 50;
        }

        public void LetsGoShelkat()
        {
            Worker();
        }

        private void Worker()
        {
            for (int i = SemkiAmount; i >= 0; i--)
            {
                if (SemkiAmount == 0)
                {
                    if (oilEnded != null)
                    {
                        oilEnded(this, new EventArgs());
                    }
                }
                SemkiAmount--;
            }
        }

    }
}
