﻿using System;


namespace основы
{
   class Program
    {
     static  void Hello()
        {
            string name = "Tom";
            int age = 34;
            double height = 1.7;
            Console.WriteLine("Имя: " + name + "  Возраст:" + age + "  Рост:" +  height);
            
        }

     static void Logical()
     {
         int x1 = 2;
         int x2 = 5;
         Console.WriteLine(x1 << x2); // сдвиг разрядов х1 на х2 разрядов

     }
       /// <summary>
     /// тернарная операция
       /// </summary>
     static void Ternar() 
     {
         int x1 = 2;
         int x2 = 5;
         Console.WriteLine("enter + or -");
         string s = Console.ReadLine();
         int z = s == "+" ? (x1 + x2) : (x1 - x2);//выполняет сложение, если условие верное и вычитание, иначе
         Console.WriteLine(z);

     }
       /// <summary>
     ///  определяет каоличество строк в массиве
       /// </summary>
     static void mass() 
     {
         int[,] mas = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 10, 11, 12 } };
         int rows = mas.GetUpperBound(0) + 1;
         int col = mas.Length / rows;
         Console.WriteLine("rows = " + rows +"\n columns = " + col);

     }
     


        static void Main(string[] args)
        {
            Hello(); // выводит на экран строку
            Logical();
            Ternar();
            mass();


            Console.ReadKey();
        }

        
    }
}
