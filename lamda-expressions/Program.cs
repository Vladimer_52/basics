﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lamda_expressions
{
    class Program
    {
        delegate int Operation(int x, int y);
        delegate void Hello();
        delegate bool IsRqual(int x);
        static void Main(string[] args)
        {
            Operation operation = (x, y) => x + y; //слева список параметров, справа выражение

            Console.WriteLine(operation(10, 10));

            Hello hello = () => Console.WriteLine("Hello");

            hello();

            int[] integers = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            int result = Sum(integers, x => x > 5); //найдем  в массиве сумму чисел больше 5

            int result2 = Sum(integers, x => x % 2 == 0); // сумма чисел кратных 2

            Console.WriteLine(result);
            Console.WriteLine(result2);
            Console.Read();
        }
        private static int Sum(int[]numbers, IsRqual func)
        {
            int result = 0;
            foreach (var i in numbers)
            {
                if(func(i))
                { result += i; }

            }
            return result;
        }
    }


  
}
