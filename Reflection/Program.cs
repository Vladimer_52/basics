﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            Type myType = typeof(User);
            Console.WriteLine(myType.ToString());
            User user = new User("Tom", 30);
            Type myType2 = user.GetType();
            Console.ReadKey();

        }
    }

    public class User
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public User(string n, int a)
        {
            Name = n;
            Age = a;
        }

        public void Display()
        {
            Console.WriteLine($"name: {Name} Age: {Age}");

        }

        public int Payment(int hours, int perhour)
        {
            return hours * perhour;
        }
    }
}
