﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delgates
{
    class Program
    {
        delegate void Message(); //создаем делегат

        delegate int Operation(int x, int y);
        static void Main(string[] args)
        {
            Message mes; // создаем переменную делегата

            if (DateTime.Now.Hour < 12) mes = GoodMorning;
            else mes = GoodEvening;

            mes.Invoke();//вызов делегата

            Operation add = Add;
            int result = add.Invoke(4, 5);
            Console.WriteLine(result);

            add += Multiply;
            result = add(4, 5);

            Console.WriteLine(result);

            Console.ReadKey();

        }

        private static int Add(int x, int y)
        {
            return x + y;
        }

        private static int Multiply(int x, int y)
        {
            return x * y;
        }
        private static void GoodMorning()
        {
            Console.WriteLine("Good morning!");
        }

        private static void GoodEvening()
        {
            Console.WriteLine("Good Evening!");
        }

    }
}
