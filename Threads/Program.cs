﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Threads
{
    class Program
    {
        static int x = 0;
        static object locker = new object();
        static Mutex mutexObj = new Mutex();
        static void Main(string[] args)
        {
            int book_num = 3;
            switch (book_num) {
                case 1:

                    Thread t = new Thread(new ParameterizedThreadStart(Count));
            t.Name = "Main";
            Console.WriteLine($"Whether the stream is running: {t.IsAlive}, \n thread priority: {t.Priority}, \nthread state: {t.ThreadState} ");
                    for (int i = 1; i < 9; i++)
                    {
                        Console.WriteLine(i * i);
                        Console.WriteLine("Main thread");
                        Thread.Sleep(300);
                    }

                    t.Start(4);
                    break;

                case 2:
                    //синхронизация потоков
                    for (int i = 0; i < 5; i++)
                    {
                        Thread myThread = new Thread(Count2);
                        myThread.Name = "Thread " + i.ToString();
                        myThread.Start();

                    }
                    break;
                case 3:
                    for (int i = 0; i < 5; i++)
                    {
                        Thread myThread = new Thread(Count3);
                        myThread.Name = "Thread " + i.ToString();
                        myThread.Start();
                    }
                    break;
        }
            Console.ReadLine();
        }

        public static void Count(object x)
        {
            for (int i = 1; i < 9; i++)
            {
                int n = (int)x;
                Console.WriteLine("second thread");
                Console.WriteLine(i * n);
                Thread.Sleep(400);
            }
        }
        public static void Count2()
        {
            try {
                Monitor.Enter(locker);
            
                x = 1;

                for (int i = 1; i < 9; i++)
                {

                    Console.WriteLine($"{Thread.CurrentThread.Name}: {x}");
                    x++;
                    Thread.Sleep(100);
                }
            }
            finally
            {
                Monitor.Exit(locker);
            }
        }
        public static void Count3()
        {
            mutexObj.WaitOne();//приостанавливает работу потока до тех пор, пока не будет получен мьютекс
            x = 1;
            for (int i = 1; i < 9; i++)
            {
                int n = (int)x;
                Console.WriteLine($"{Thread.CurrentThread.Name}:{x}");
                x++;
                Thread.Sleep(100);
            }
            mutexObj.ReleaseMutex();//освобождает мьютекс
        }
    }
}
